\documentclass[compress,xcolor=table]{beamer}
\usepackage{lmodern}
\usepackage{tabularx,booktabs}

\mode<presentation>
{
	\usetheme{Madrid}      % or try Darmstadt, Madrid, Warsaw, ...
	\usecolortheme{seahorse} % or try albatross, beaver, crane, whale ...
	\usefonttheme{serif}  % or try serif, structurebold, ...
	\setbeamertemplate{navigation symbols}{}
	\setbeamertemplate{caption}[numbered]
} 

\makeatletter
\setbeamertemplate{headline}{%
	\begin{beamercolorbox}[ht=2.25ex,dp=3.75ex]{section in head/foot}
		\insertnavigation{\paperwidth}
	\end{beamercolorbox}%
}%
\makeatother

\makeatletter
\newenvironment{withoutheadline}{
	\setbeamertemplate{headline}[default]
	\def\beamer@entrycode{\vspace*{-\headheight}}
}{}
\makeatother

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{textpos}
\newcommand\citem[1]{\item[{[#1]}] }

%\newcommand\pro{\item[\textcolor{green}{$+$}]}
%\newcommand\con{\item[\textcolor{red}{$-$}]}

\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{rotating}

\lstset
{
	language=[LaTeX]TeX,
	breaklines=true,
	basicstyle=\tt\scriptsize,
	%commentstyle=\color{green}
	keywordstyle=\color{blue},
	%stringstyle=\color{black}
	identifierstyle=\color{orange},
}

\newcommand{\backupbegin}{
	\newcounter{finalframe}
	\setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
	\setcounter{framenumber}{\value{finalframe}}
}

\newenvironment<>{varblock}[2][.9\textwidth]{%
	\setlength{\textwidth}{#1}
	\begin{actionenv}#3%
		\def\insertblocktitle{#2}%
		\par%
		\usebeamertemplate{block begin}}
	{\par%
		\usebeamertemplate{block end}%
\end{actionenv}}

%\usepackage{outlines}
\setbeamercolor{itemize item}{fg=red,bg=white}

\usepackage{multirow}
\usepackage{caption}
\usepackage{subcaption}

%%% TITLE
\title[FGAN-I-007]{Integration of Network Simulators in Closed-Loop Automation for Future Networks: \\Work done and ways forward\footnote{\cite{p1} Wilhelmi et al. ``Usage of Network Simulators in ML-Assisted 5G/6G Networks'', \textit{IEEE Wireless Communications Magazine}.}}
\author[Francesc Wilhelmi]{\includegraphics[width=\textwidth,height=0.13\textheight,keepaspectratio]{img/cttc_logo.jpg}\\~\\Francesc Wilhelmi}
\institute[CTTC]{fwilhelmi@cttc.cat}
\date[February, 2021]{1st Meeting, February, 2021}

\AtBeginSection[]
{
\begin{frame}<beamer>
	\frametitle{Outline}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\begin{document}

\begin{withoutheadline}
	\begin{frame}
		\titlepage
	\end{frame}
\end{withoutheadline}

\begin{frame}{Table of contents} % and our simple frame
	\tableofcontents
\end{frame}

%%% Purpose / preliminaries
\section[Motivation]{Motivation}

\subsection{}
\begin{frame}{Motivation}
	\begin{block}{Some important problems}
		\begin{enumerate}
			\item Concerns about AI/ML-driven autonomous operation
			\item Consequences of learning through online methods
			\item Lack of training data
		\end{enumerate}
	\end{block}
	\begin{exampleblock}{Solution proposal}
		\begin{enumerate}			
			\item Validate ML models
			\item Train models
			\item Generate synthetic training data
			\\... in a \textbf{safe environment}
		\end{enumerate}
	\end{exampleblock}	
\end{frame}

%%% Background publication
\section[Background]{Background}

\subsection{}
\begin{frame}{Simulators in ML-aware 5G/6G communications}
	\begin{block}{Scope}
		\begin{enumerate}
			\item Focus on ML Sandbox \cite{p2}
			\item Particular case of network simulators
			\item Proof-of-concept implementation
		\end{enumerate}
	\end{block}
	\begin{exampleblock}{Integration Aspects}
		\begin{enumerate}
			\item The importance of metadata % plethora of simulators
			\item Portability
			\item Interoperability
			\item Pluggable ML models
		\end{enumerate}
	\end{exampleblock}	
\end{frame}



\subsection{}
\begin{frame}{ML Sandbox in ITU-T ML Architecture}
	\begin{figure}
		\includegraphics[width=\textwidth,height=0.7\textheight,keepaspectratio]{img/architecture_example}\caption{ITU-T's logical ML architecture for future networks.}
	\end{figure}
\end{frame}


%%% PoC
\section[PoC]{Proof-of-Concept Implementation}

% Challenges
\subsection{}
\begin{frame}{Scenario Overview}
		\begin{figure}
			\includegraphics[width=\textwidth,height=0.4\textheight,keepaspectratio]{img/testbed2.pdf}
		\end{figure}	
		\begin{center}
			\begin{minipage}{8cm}
				\begin{block}{}
					\centering
					\begin{itemize}
						\item Transmission Power Control in WLANs
						\item Online learning approach
						\item Training done in the simulation domain
					\end{itemize}
				\end{block}
			\end{minipage}
		\end{center}
		\let\thefootnote\relax\footnotetext{\scriptsize Barrachina-Muñoz, S., Wilhelmi, F., Selinis, I., \& Bellalta, B. (2019). ``Komondor: a Wireless Network Simulator for Next-Generation High-Density WLANs'', \emph{in 2019 Wireless Days (WD).} IEEE, pp. 1–8.}	
%Gaps
% 1) Interfaces
% 2) From reality to simulation (and vice-versa) which simulator you pick, how do you select the input parameters according to the testbed, which ML model you select, which optimization goals you set, etc.
% 3) Self-adaptation
\end{frame}

\begin{frame}{Simulation domain}
\begin{columns}
	\begin{column}{5cm}
		\begin{block}{Procedure}
			\begin{enumerate}
				\item Prepare the simulation %based on data extracted from the testbed (e.g., calibrate path-loss)
				\item Execute online learning procedure
				\item Collect the results
			\end{enumerate}
		\end{block}
	\end{column}
	%\pause
	\begin{column}{6.5cm}
		\begin{figure}
			\includegraphics[width=\textwidth,height=0.5\textheight,keepaspectratio]{img/throughput_evolution_komondor.png}
		\end{figure}
		\vspace{-0.5cm}
		\scriptsize\begin{itemize}
			\item Decentralized multi-armed bandit approach
			\item Allowed tx. power: \{5, 7, 12, 17, 23\} dBm
			\item More details at \url{https://github.com/fwilhelmi/usage_of_simulators_in_future_networks}
		\end{itemize}
	\end{column}
\end{columns}
\end{frame}

\begin{frame}{From simulation to testbed}
\begin{columns}
	\begin{column}{6cm}
		\begin{figure}
			\includegraphics[width=\textwidth,height=0.5\textheight,keepaspectratio]{img/AP1_AP2_ALL.png}
		\end{figure}
	\end{column}
	\begin{column}{6cm}
		\begin{figure}
			\includegraphics[width=\textwidth,height=0.5\textheight,keepaspectratio]{img/boxplotbps.png}
		\end{figure}
	\end{column}
\end{columns}

\begin{center}
	\begin{minipage}{9.5cm}
		\begin{block}{}
			\centering
			\begin{itemize}
				\item Default configuration: \textbf{23 dBm}
				\item Best configuration found by the simulator: \textbf{7 dBm}
			\end{itemize}
		\end{block}
	\end{minipage}
\end{center}

\end{frame}

\begin{frame}{On the accuracy of simulation results}
	\begin{block}{Some insights}
		\begin{itemize}
			\item Extrapolation of simulation results (qualitative vs quantitative)
			\item Accuracy vs Execution time
			\item Under-researched subject
		\end{itemize}
	\end{block}
%\pause
	\begin{figure}
		\includegraphics[width=\textwidth,height=0.45\textheight,keepaspectratio]{img/test_sim_time_vs_accuracy.png}
	\end{figure}
\end{frame}

%%% Discussion
\section[Discussion]{Open Issues \& Discussion Points}

\subsection{}
\begin{frame}{Potential study points}

%The following open aspects are identified as keys for the development of closed-loop, autonomous communications systems:
\begin{enumerate}
	\item Define and implement standardized or open-source interfaces
	\item Define demand mapping procedures: from ML use case to instructions %to convert requirements from ML use cases into specific instructions that network components can understand and execute %Demand mapping can be complemented for new use cases through the analysis of data patterns and ML pipeline output. 
	\item Enable the aim of “exploration and experimentation” mentioned in [FG-AN ToR] 
	\item Assess the accuracy of synthetic data for training % (e.g., through Generative Adversarial Networks).
	\item Compare optimization models and simulation tools in terms of output accuracy, execution time, or computation needs
\end{enumerate}

\end{frame}

\section{}
\begin{frame}{Any questions?}
\begin{figure}
	\includegraphics[width=\textwidth,height=0.4\textheight,keepaspectratio]{img/question_mark.png}
\end{figure}

\begin{center}	
	\footnotesize
	\textbf{Francesc Wilhelmi, Ph.D.}\\
	\textcolor{blue}{fwilhelmi@cttc.cat}\\
	Centre Tecnològic de Telecomunicacions de Catalunya (CTTC)
\end{center}

\end{frame}

% References 
\begin{frame}
\frametitle{References}
\footnotesize{
	\begin{thebibliography}{99}
				
		\bibitem[FW20]{p1} Wilhelmi, F., Carrascosa, M., Cano, C., Jonsson, A., Ram, V., \& Bellalta, B. (2020). \newblock ``Usage of Network Simulators in Machine-Learning-Assisted 5G/6G Networks". \emph{arXiv preprint arXiv:2005.08281}.
		
		\bibitem[ML5G-I-238]{p2} FG ML5G specification: \newblock``Machine Learning Sandbox for future networks including IMT-2020: requirements and architecture framework"
		
		\bibitem[FG-AN ToR]{p3} Terms of Reference: \newblock ITU-T Focus Group on “Autonomous Networks” (FG-AN) \url{https://www.itu.int/en/ITU-T/focusgroups/an/Documents/FG-AN_Terms_of_Reference.pdf?csf=1&e=dRZrwa}
				
	\end{thebibliography}
}
\end{frame}


% Backup slides here
\backupbegin
% ...
\backupend

\end{document}