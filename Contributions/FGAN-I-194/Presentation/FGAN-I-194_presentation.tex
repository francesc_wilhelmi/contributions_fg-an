\documentclass[compress,xcolor=table]{beamer}
\usepackage{lmodern}
\usepackage{tabularx,booktabs}

\mode<presentation>
{
	\usetheme{Madrid}      % or try Darmstadt, Madrid, Warsaw, ...
	\usecolortheme{seahorse} % or try albatross, beaver, crane, whale ...
	\usefonttheme{serif}  % or try serif, structurebold, ...
	\setbeamertemplate{navigation symbols}{}
	\setbeamertemplate{caption}[numbered]
} 

\makeatletter
\setbeamertemplate{headline}{%
	\begin{beamercolorbox}[ht=2.25ex,dp=3.75ex]{section in head/foot}
		\insertnavigation{\paperwidth}
	\end{beamercolorbox}%
}%
\makeatother

\makeatletter
\newenvironment{withoutheadline}{
	\setbeamertemplate{headline}[default]
	\def\beamer@entrycode{\vspace*{-\headheight}}
}{}
\makeatother

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{textpos}
\newcommand\citem[1]{\item[{[#1]}] }

%\newcommand\pro{\item[\textcolor{green}{$+$}]}
%\newcommand\con{\item[\textcolor{red}{$-$}]}

\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{rotating}

\lstset
{
	language=[LaTeX]TeX,
	breaklines=true,
	basicstyle=\tt\scriptsize,
	%commentstyle=\color{green}
	keywordstyle=\color{blue},
	%stringstyle=\color{black}
	identifierstyle=\color{orange},
}

\newcommand{\backupbegin}{
	\newcounter{finalframe}
	\setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
	\setcounter{framenumber}{\value{finalframe}}
}

\newenvironment<>{varblock}[2][.9\textwidth]{%
	\setlength{\textwidth}{#1}
	\begin{actionenv}#3%
		\def\insertblocktitle{#2}%
		\par%
		\usebeamertemplate{block begin}}
	{\par%
		\usebeamertemplate{block end}%
\end{actionenv}}

%\usepackage{outlines}
\setbeamercolor{itemize item}{fg=red,bg=white}

\usepackage{multirow}
\usepackage{caption}
\usepackage{subcaption}

%%% TITLE
\title[FGAN-I-194]{Data Markets and Collaborative Model Training with Blockchain: A use case on server-less FL\footnote{Wilhelmi, F., Giupponi, L., \& Dini, P. (2021). Blockchain-enabled Server-less Federated Learning. arXiv preprint arXiv:2112.07938.}}
\author[Wilhelmi, Giupponi \& Dini]{\includegraphics[width=\textwidth,height=0.13\textheight,keepaspectratio]{img/cttc_logo.jpg}\\~\\Francesc Wilhelmi, Lorenza Giupponi \& Paolo Dini}
\institute[CTTC]{fwilhelmi@cttc.cat}
\date[January, 2022]{6th Meeting, January, 2022}

\AtBeginSection[]
{
\begin{frame}<beamer>
	\frametitle{Outline}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\begin{document}

\begin{withoutheadline}
	\begin{frame}
		\titlepage
	\end{frame}
\end{withoutheadline}

\begin{frame}{Table of contents} % and our simple frame
	\tableofcontents
\end{frame}

%%% Purpose / preliminaries
\section[Contribution]{Contribution}

\subsection{}
\begin{frame}{Highlights of this contribution}
	\begin{block}{Summary of contributions (paper)}
		\begin{enumerate}
			\item Server-less realization of FL, enabled by Blockchain
			\item Study the asynchronous operation of FL
		\end{enumerate}
	\end{block}
\pause
	\begin{exampleblock}{Relevant points (for the FG)}
		\begin{enumerate}			
			\item Blockchain properties
			\item Decentralization of computation
			\item Exchange resources (e.g., data models) via Blockchain
			\item Open aspects for the adoption of Blockchain
		\end{enumerate}
	\end{exampleblock}	
\end{frame}

%%% Purpose / preliminaries
\section[Intro. Blockchain]{Introduction on Blockchain}

\subsection{}
\begin{frame}{Overview}
	\begin{enumerate}
		\item Decentralized database organized in (chained) blocks
		\item Maintained by Blockchain nodes (e.g., miners, validators)
		\item P2P communication
	\end{enumerate}
	\begin{figure}
		\includegraphics[width=.7\textwidth,keepaspectratio]{img/blockchain_summary}
	\end{figure}
\end{frame}

\subsection{}
\begin{frame}{Blockchain key properties}
\begin{exampleblock}{Blockchain key properties}
	\begin{enumerate}
		\item \textbf{Decentralized:} operates under consensus, no third party required
		\item \textbf{Resilience:} no single point of failure
		\item \textbf{Security:} hard to hack + validation of transactions via public/private keys
		\item \textbf{Transparency:} transactions available to everyone
		\item \textbf{Immutability:} information cannot be tampered
		\item \textbf{Efficiency:} near-real-time operations %	mobile network sharing solutions require long negotiation processes that hardly fit within short time-to-market deployments of the 5G use-cases
	\end{enumerate}
\end{exampleblock}
%
\pause
\begin{center}
	\begin{minipage}{7cm}
		\begin{block}{}
			\centering
				\textbf{$\uparrow$ Trust} (to enable automation)
		\end{block}
	\end{minipage}
\end{center}
\end{frame}

\subsection{}
\begin{frame}{Increasing interest on Blockchain for communications}
\begin{block}{Relevant use cases}
	\begin{enumerate}
		\item \textbf{Autonomous driving:} securing communications~\cite{bc_autonomous_driving}, vehicular ecosystem~\cite{bc_automotive}
		\item \textbf{Internet of Things:} secure data sharing~\cite{bc_iot, bc_iot2}
		\item \textbf{Identity management:}  authentication in D2D communications~\cite{beran}
		\item \textbf{Network sharing:} brokering~\cite{bc_broker}, competitiveness~\cite{bc_business}
		\item \textbf{RAN management:} spectrum sharing~\cite{bc_spectrum}, service provisioning~\cite{bc_spectrum2}
	\end{enumerate}
\end{block}
\pause
\begin{center}
	\begin{minipage}{9cm}
		\begin{alertblock}{}
			\centering
			Exponential adoption in telecom\footnote{The $\sim$50\% of papers including keywords \textit{Blockchain} and \\\textit{Telecommunications} have been published since 2020}
		\end{alertblock}
	\end{minipage}
\end{center}
\end{frame}

\subsection{}
\begin{frame}{A use case: BC \& O-RAN (previous work)}
\begin{figure}
	\includegraphics[width=\textwidth,keepaspectratio]{img/blockchain_ran-ecosystem}%\caption{ITU-T's logical ML architecture for future networks.}
\end{figure}
\end{frame}

%%% Background publication
\section[Blockchain \& Federated Learning]{Blockchain-enabled Server-less FL}

\subsection{}
\begin{frame}{System model}
\begin{columns}
	\begin{column}{5cm}
			\begin{block}{Server-less FL}
				\begin{enumerate}
					\item Clients submit local model updates to the BC
					\item Local updates are validated by miners
					\item Clients (or edge nodes) aggregate model updates
				\end{enumerate}
			\end{block}
	\end{column}
	\begin{column}{7cm}
		\begin{figure}
		\includegraphics[width=\textwidth,keepaspectratio]{img/blockchain_federated_learning2}
		\end{figure}
	\end{column}
\end{columns}
\end{frame}

\subsection{}
\begin{frame}{Synchronous vs Asynchronous operation}
\begin{columns}
	\begin{column}{5.5cm}
		\begin{block}{Synchronous FL}
			\begin{enumerate}
				\item Orchestrated (e.g., server)
				\item Scheduled model updates
				\item Blocking process
			\end{enumerate}
		\end{block}
	\end{column}
	\begin{column}{5.5cm}
		\begin{exampleblock}{Asynchronous FL}
			\begin{enumerate}
				\item No orchestration
				\item Unordered model updates 
				\item Suitable for sever-less FL
			\end{enumerate}
		\end{exampleblock}
	\end{column}
\end{columns}
		\begin{figure}
	\includegraphics[width=.6\textwidth,keepaspectratio]{img/sync_vs_async_diagram}
\end{figure}
\end{frame}

\subsection{}
\begin{frame}{Role of Blockchain in ML}
	\begin{block}{Boost collaboration}
		\begin{enumerate}
			\item Buy/sell/exchange training data (solve data gap)
			\item In federated settings, \textit{Training-as-a-Service}
		\end{enumerate}
	\end{block}
	\begin{exampleblock}{Reliable data sources}
		\begin{enumerate}
			\item Trusted collaborators, consensus 
			\item Data validation procedures (digital signature schemes)
		\end{enumerate}
	\end{exampleblock}
	\begin{alertblock}{Accelerate innovation}
		\begin{enumerate}
			\item Open, decentralized markets
			\item More competition
		\end{enumerate}
	\end{alertblock}
\end{frame}


%%% Discussion
\section[Discussion]{Discussion}

\subsection{}
\begin{frame}{Challenges \& Opportunities}
	\begin{itemize}
		\item \textbf{Demand mapping:} mechanisms map network-related info into SCs (from real world to digital world)
		\begin{itemize}
			\item Network KPIs (e.g., VNF resources)
			\item User needs/requirements (sometimes non-tangible)
		\end{itemize}
		%
		\item \textbf{Dynamic world:} price fluctuations, service availability, etc.
		\begin{itemize}
			\item External data sources are needed (e.g., sensors, organizations)
			\item Interoperability aspects (e.g., APIs to services)
		\end{itemize}
		%
		\item \textbf{Need for standards:} lack of standardization in BC for telecommunications (data formats, interfaces, protocols, etc.)\footnote{ITU has already defined some specifications on BC terminology, use cases, reference architecture, etc.}
		\begin{itemize}
			\item Potential collaboration with \textit{SG17: Security}, which already established the \textit{FG-DLT}
		\end{itemize}
		%\item \textbf{Scalability:} an increase in the number of BC users and transactions can represent both a communication and a storage issue
	\end{itemize}
\end{frame}

\section{}
\begin{frame}{Any questions?}
\begin{figure}
	\includegraphics[width=\textwidth,height=0.4\textheight,keepaspectratio]{img/question_mark.png}
\end{figure}

\begin{center}	
	\footnotesize
	\textbf{Francesc Wilhelmi, Ph.D.}\\
	\textcolor{blue}{fwilhelmi@cttc.cat}\\
	Centre Tecnològic de Telecomunicacions de Catalunya (CTTC)
\end{center}

\end{frame}

% References 
\begin{frame}
\frametitle{References (I)}
\footnotesize{
	\begin{thebibliography}{99}				
		\bibitem[1]{bc_autonomous_driving} Rathee, G., Sharma, A., Iqbal, R., Aloqaily, M., Jaglan, N., \& Kumar, R. (2019). A blockchain framework for securing connected and autonomous vehicles. Sensors, 19(14), 3165.		
		\bibitem[2]{bc_automotive} Dorri, A., Steger, M., Kanhere, S. S., \& Jurdak, R. (2017). Blockchain: A distributed solution to automotive security and privacy. IEEE Communications Magazine, 55(12), 119-125.		
		\bibitem[3]{bc_iot} Shafagh, H., Burkhalter, L., Hithnawi, A., \& Duquennoy, S. (2017, November). Towards blockchain-based auditable storage and sharing of iot data. In Proceedings of the 2017 on cloud computing security workshop (pp. 45-50).		
		\bibitem[4]{bc_iot2} Asheralieva, Alia, and Dusit Niyato. "Distributed dynamic resource management and pricing in the IoT systems with blockchain-as-a-service and UAV-enabled mobile edge computing." IEEE Internet of Things Journal 7.3 (2019): 1974-1993.
	\end{thebibliography}
}
\end{frame}

\begin{frame}
\frametitle{References (II)}
\footnotesize{
	\begin{thebibliography}{99}				
		\bibitem[5]{beran} Xu, Hao, Lei Zhang, and Elaine Sun. "BE-RAN: Blockchain-enabled Open RAN with Decentralized Identity Management and Privacy-Preserving Communication." arXiv preprint arXiv:2101.10856 (2021).				
		\bibitem[6]{bc_broker} Togou, Mohammed Amine, et al. "A distributed blockchain-based broker for efficient resource provisioning in 5g networks." 2020 International Wireless Communications and Mobile Computing (IWCMC). IEEE, 2020.		
		\bibitem[7]{bc_business} Yrjölä, Seppo. "How could blockchain transform 6G towards open ecosystemic business models?." 2020 IEEE International Conference on Communications Workshops (ICC Workshops). IEEE, 2020.		
		\bibitem[8]{bc_spectrum} Maksymyuk, Taras, et al. "Blockchain-empowered framework for decentralized network management in 6G." IEEE Communications Magazine 58.9 (2020): 86-92.		
		\bibitem[9]{bc_spectrum2} Ling, Xintong, et al. "Blockchain radio access network (B-RAN): Towards decentralized secure radio access paradigm." IEEE Access 7 (2019): 9714-9723.				
	\end{thebibliography}
}
\end{frame}


% Backup slides here
\backupbegin

\subsection{}
\begin{frame}{Blockchain queue model}
\begin{figure}
	\includegraphics[width=\textwidth,keepaspectratio]{img/batch_service_queue2}
\end{figure}
\end{frame}

\subsection{}
\begin{frame}{Some results}
\begin{figure}
	\includegraphics[width=\textwidth,keepaspectratio]{img/eval_accuracy_time_iid}
\end{figure}
\begin{figure}
	\includegraphics[width=\textwidth,keepaspectratio]{img/eval_accuracy_time}
\end{figure}
\end{frame}



\backupend

\end{document}