\documentclass[compress,xcolor=table]{beamer}
\usepackage{lmodern}
\usepackage{tabularx,booktabs}

\mode<presentation>
{
	\usetheme{Madrid}      % or try Darmstadt, Madrid, Warsaw, ...
	\usecolortheme{seahorse} % or try albatross, beaver, crane, whale ...
	\usefonttheme{serif}  % or try serif, structurebold, ...
	\setbeamertemplate{navigation symbols}{}
	\setbeamertemplate{caption}[numbered]
} 

\makeatletter
\setbeamertemplate{headline}{%
	\begin{beamercolorbox}[ht=2.25ex,dp=3.75ex]{section in head/foot}
		\insertnavigation{\paperwidth}
	\end{beamercolorbox}%
}%
\makeatother

\makeatletter
\newenvironment{withoutheadline}{
	\setbeamertemplate{headline}[default]
	\def\beamer@entrycode{\vspace*{-\headheight}}
}{}
\makeatother

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{textpos}
\newcommand\citem[1]{\item[{[#1]}] }

%\newcommand\pro{\item[\textcolor{green}{$+$}]}
%\newcommand\con{\item[\textcolor{red}{$-$}]}

\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{rotating}

\lstset
{
	language=[LaTeX]TeX,
	breaklines=true,
	basicstyle=\tt\scriptsize,
	%commentstyle=\color{green}
	keywordstyle=\color{blue},
	%stringstyle=\color{black}
	identifierstyle=\color{orange},
}

\newcommand{\backupbegin}{
	\newcounter{finalframe}
	\setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
	\setcounter{framenumber}{\value{finalframe}}
}

\newenvironment<>{varblock}[2][.9\textwidth]{%
	\setlength{\textwidth}{#1}
	\begin{actionenv}#3%
		\def\insertblocktitle{#2}%
		\par%
		\usebeamertemplate{block begin}}
	{\par%
		\usebeamertemplate{block end}%
\end{actionenv}}

%\usepackage{outlines}
\setbeamercolor{itemize item}{fg=red,bg=white}

\usepackage{multirow}
\usepackage{caption}
\usepackage{subcaption}

%%% TITLE
\title[FGAN-I-nw-sharing-bc]{Blockchain-enabled Network Sharing for O-RAN\footnote{\cite{p1} Giupponi, L., \& Wilhelmi, F. (2021). Blockchain-enabled Network Sharing for O-RAN. arXiv preprint arXiv:2107.02005.}}
\author[L. Giupponi \& F. Wilhelmi]{\includegraphics[width=\textwidth,height=0.13\textheight,keepaspectratio]{img/cttc_logo.jpg}\\~\\Lorenza Giupponi \& Francesc Wilhelmi}
\institute[CTTC]{fwilhelmi@cttc.cat}
\date[September, 2021]{4th Meeting, September, 2021}

\AtBeginSection[]
{
\begin{frame}<beamer>
	\frametitle{Outline}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\begin{document}

\begin{withoutheadline}
	\begin{frame}
		\titlepage
	\end{frame}
\end{withoutheadline}

\begin{frame}{Table of contents} % and our simple frame
	\tableofcontents
\end{frame}

%%% Purpose / preliminaries
\section[Introduction]{Introduction}

\subsection{}
\begin{frame}{Network sharing}
	\begin{block}{Current situation}
		\begin{enumerate}
			\item Unclear ARPU increment for 5G deployments
			\item Concentration of costs in the RAN
			\item Need for cutting CAPEX/OPEX costs
		\end{enumerate}
	\end{block}
	\begin{exampleblock}{RAN sharing as a promising solution}
		\begin{enumerate}			
			\item Increase competitiveness
			\item Attract new players (OTT SP, verticals, private networks...)
			\item Take advantage of network virtualization and openness (O-RAN)
		\end{enumerate}
	\end{exampleblock}	
\end{frame}

\subsection{}
\begin{frame}{Proposal}
%\begin{columns}
%	\begin{column}{5cm}
		\begin{block}{Blockchain for autonomous network management}
			\begin{enumerate}
				\item Key properties: Immutability, decentralization, transparency
				\item Removes the need for costly intermediaries
				\item Automation of the network management
			\end{enumerate}
		\end{block}
%	\end{column}
%	\begin{column}{5cm}
		\begin{figure}
			\includegraphics[width=.7\textwidth,keepaspectratio]{img/blockchain_summary}%\caption{ITU-T's logical ML architecture for future networks.}
		\end{figure}
%	\end{column}
%\end{columns}
\end{frame}

\subsection{}
\begin{frame}{BC-enabled O-RAN scenario}
\begin{figure}
	\includegraphics[width=\textwidth,keepaspectratio]{img/blockchain_ran-ecosystem}%\caption{ITU-T's logical ML architecture for future networks.}
\end{figure}
\end{frame}

%%% Background publication
\section[Architecture]{Architectural Framework}

\subsection{}
\begin{frame}{O-RAN Architecture}
	\begin{block}{O-RAN components~\cite{p2}}
		\begin{enumerate}
			\item System management and orchestration (SMO)
			\item O-RAN centralized unit (O-CU)
			\item O-RAN decentralized unit (O-DU)
			\item Radio intelligent controller (RIC)
		\end{enumerate}
	\end{block}
	\begin{exampleblock}{RAN sharing use case~\cite{p3}}
		\begin{enumerate}
			\item Configure shared network resources independently
			\item Monitoring QoS parameters
			\item Favor flexibility	and optimization of resources
		\end{enumerate}
	\end{exampleblock}
\end{frame}

\subsection{}
\begin{frame}{BC for O-RAN}
	\begin{block}{Existing literature}
		\begin{itemize}
		\item O-RAN-based architecture to conduct zero-trust mutual authentication with specialized hardware~\cite{p4}
		\begin{itemize}
			\item Currently being discussed in O-RAN's Security Focus Group (SFG)
		\end{itemize}
		\item Other works on BC protocols for resource sharing in 5G/6G~\cite{p5,p6}
	\end{itemize}
	\end{block}
	\begin{alertblock}{Our work}
		\begin{itemize}
			\item We focus on RAN sharing and apply BC to automate, accelerate, and secure the trade of resources
			\item We extend the O-RAN architecture to automate the RAN sharing use case
			\item We focus on practical implementation aspects
		\end{itemize}
	\end{alertblock}
\end{frame}

\subsection{}
\begin{frame}{BC-enabled O-RAN Architecture}
\begin{figure}
	\includegraphics[width=\textwidth,keepaspectratio]{img/functionalarchitecture2}%\caption{ITU-T's logical ML architecture for future networks.}
\end{figure}
\end{frame}

\subsection{}
\begin{frame}{RAN sharing mechanisms}

\begin{columns}
	\begin{column}{5.5cm}
		\begin{block}{Marketplace-oriented}
			\begin{itemize}
				\item Published offers
				\item Low flexibility
				\item High efficiency
				\item Low overhead
			\end{itemize}
		\end{block}
	\end{column}
	\begin{column}{5.5cm}
		\begin{exampleblock}{Auction-based}
			\begin{itemize}
				\item Bidding system
				\item High flexibility
				\item Poor scalability
				\item High overhead
			\end{itemize}
		\end{exampleblock}
	\end{column}
\end{columns}

\end{frame}

\subsection{}
\begin{frame}{Flow diagram - Auction}
\begin{figure}
	\includegraphics[width=.7\textwidth,keepaspectratio]{img/marketplace}%\caption{ITU-T's logical ML architecture for future networks.}
\end{figure}
\end{frame}

\subsection{}
\begin{frame}{Flow diagram - Marketplace}
\begin{figure}
	\includegraphics[width=.7\textwidth,keepaspectratio]{img/auction}%\caption{ITU-T's logical ML architecture for future networks.}
\end{figure}
\end{frame}

%%% Discussion
\section[Discussion]{Discussion}

\subsection{}
\begin{frame}{Opportunities}
	\begin{itemize}
		\item \textbf{Automated management:} remove long interactions with third parties% in the negotiation for sharing resources
		%
		\item \textbf{Resources efficiency:} higher network capacity, more coverage, and improved users' satisfaction.
		%
		\item \textbf{Competitiveness:} attract more investments in the network
		%
		\item \textbf{Auditablity:} improved trust and transparency in RAN sharing%. This is an important aspect to take into account, considering the recent issues raised by, e.g., Ericsson warning on O-RAN security~\cite{boswell2020security}. 
	\end{itemize}
\end{frame}

\subsection{}
\begin{frame}{Challenges}
\begin{itemize}
	\item \textbf{Communication overhead:} increases with the number of participants and the duration of the requested services (accurate short-term requests vs long-term fixed contracts).
	%
	\item \textbf{Transaction confirmation latency:} the distribution of information across the BC adds delay for instantiating RAN functions
	%
	\item \textbf{Stability:} the stability of a BC is strongly tied to the network consensus and game-theoretical aspects may motivate selfish behaviors %Forks may incur additional transaction confirmation delay. 
	%
	\item \textbf{Scalability:} an increase in the number of BC users and transactions can represent both a communication and a storage issue%. Besides, BC is originally limited by the block size and the mining difficulty (see, e.g., the case of Bitcoin), which limits the effective transactions rate. 
\end{itemize}
\end{frame}

\section{}
\begin{frame}{Any questions?}
\begin{figure}
	\includegraphics[width=\textwidth,height=0.4\textheight,keepaspectratio]{img/question_mark.png}
\end{figure}

\begin{center}	
	\footnotesize
	\textbf{Francesc Wilhelmi, Ph.D.}\\
	\textcolor{blue}{fwilhelmi@cttc.cat}\\
	Centre Tecnològic de Telecomunicacions de Catalunya (CTTC)
\end{center}

\end{frame}

% References 
\begin{frame}
\frametitle{References}
\footnotesize{
	\begin{thebibliography}{99}
				
		\bibitem[1]{p1} Giupponi, L., \& Wilhelmi, F. (2021). \newblock Blockchain-enabled Network Sharing for O-RAN. \emph{arXiv preprint arXiv:2107.02005}.
		
		\bibitem[2]{p2} O-RAN Alliance, “O-RAN Architecture Description 3.0,” \url{https://www.o-ran.org/resources}, [Accessed on 31 August 2021].
		
		\bibitem[3]{p3} O-RAN Alliance, “Use case analysis report". O-RAN.WG1.Use-Cases-Analysis-Report-v04.00 [Accessed on 31 August 2021].
		
		\bibitem[4]{p4} Xu, H., Zhang, L., \& Sun, E. (2021). “BE-RAN: Blockchain-enabled RAN with Decentralized Identity Management and Privacy-Preserving Communication".\emph{ arXiv e-prints, arXiv-2101}.
		
		\bibitem[5]{p5} “Maksymyuk, T., Gazda, J., Volosin, M., Bugar, G., Horvath, D., Klymash, M., \& Dohler, M. (2020). Blockchain-empowered framework for decentralized network management in 6G". \emph{IEEE Communications Magazine, 58(9), 86-92.}
		
		\bibitem[6]{p6} “Togou, M. A., Bi, T., Dev, K., McDonnell, K., Milenovic, A., Tewari, H., \& Muntean, G. M. (2020). DBNS: A distributed blockchain-enabled network slicing framework for 5G networks". \emph{IEEE Communications Magazine, 58(11), 90-96.}
				
	\end{thebibliography}
}
\end{frame}


% Backup slides here
\backupbegin
% ...
\backupend

\end{document}